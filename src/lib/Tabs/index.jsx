import React from 'react';

const Tab = (props) => {
	return <div className="_Tabs__Tab">
		{props.children}
	</div>
};

export default class extends React.Component {
	static defaultProps = {
		tabs: []
	};
	renderTab = (tab, key) => {
		console.log('TAB', tab, key);
		return <div>TAB</div>
	};
	render() {
		return <div className="_Tabs">
			<div className="_Tabs__Control">

			</div>
			{this.props.children.map(this.renderTab)}
		</div>
	}
};