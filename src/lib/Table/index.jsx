import React from 'react';
import classNames from 'classnames';

import Column from './Column';

import './index.styl';

export default class Table extends React.Component {
	columns = [];
	renderColumn = (column, key) => {
		return <Column key={key} width={column.width}>{column.title}</Column>
	};
	renderRow = (data, key) => {
		return <div className="_Table__Row" key={key}>
			{this.columns.map((column, key) => {
				return <div className="_Table__Cell" key={key}>
					{typeof column.children === 'function' ? column.children(data, key) : column.children}
				</div>
			})}
		</div>
	};
	render() {
		this.columns = React.Children.map( //@TODO: Only column component
			this.props.children,
			child => {
				return child && {
					title: child.props.title,
					children: child.props.children,
					width: child.props.width || 'auto'
				}
			}
		);
		return <div className={classNames('_Table', this.props.className)}>
			<div className="_Table__Header">
				{this.columns.map(this.renderColumn)}
			</div>
			<div className="_Table__Body">
				{this.props.data.map(this.renderRow)}
			</div>
		</div>
	}
}

Table.defaultProps = {
	data: []
};

Table.Column = Column;