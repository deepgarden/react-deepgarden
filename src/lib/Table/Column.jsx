const Column = props => {
	return <div className="_Table__Column" style={{width: props.width}}>
		{props.children}
	</div>
};

export default Column;