import React from 'react';

import classNames from 'classnames';

import './index.styl';

const Button = props => {
	const {className, ...restProps} = props;
	const Component = props.href ? 'a' : 'div';
	return <Component {...restProps} className={classNames('_Button', className)}>
		{props.children}
	</Component>
};

export default Button;