import React from 'react';
import classNames from 'classnames';

import './index.styl';

const types = {
	default: '-Default',
	success: '-Success',
	notice: '-Notice',
	warning: '-Warning',
	danger: '-Danger'
};

const Badge = props => {
	const className = types[props.type || 'default'];
	return <div className={classNames('_Badge', className, props.className)}>
		{props.children}
	</div>
};

export default Badge;