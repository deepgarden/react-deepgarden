import React from 'react';
import ReactDOM from 'react-dom';

import classNames from 'classnames';

import './index.styl';

export default class Modal extends React.Component {
	triggerClose = () => {
		this.props.onClose && this.props.onClose();
	};
	handleClick = e => {
		if (e.target === this.overlay) {
			this.triggerClose();
		}
	};
	componentWillMount() {
		this.node = document.createElement('div');
		document.body.appendChild(this.node);
	}
	componentWillUnmount() {
		document.body.removeChild(this.node);
	}
	renderModal = () => {
		return <div className="_Modal__Overlay" onClick={this.handleClick} ref={overlay => this.overlay = overlay}>
			<div className={classNames('_Modal', this.props.className)}>
				{this.props.title && <div className="_Modal__Header">
					<div className="_Modal__Title">
						{this.props.title}
					</div>
				</div>}
				<div className="_Modal__Body">
					{this.props.children}
				</div>
			</div>
			<div className="_Modal__Close"/>
		</div>
	};
	render() {
		return ReactDOM.createPortal(
			this.renderModal(this.props),
			this.node
		);
	}
}