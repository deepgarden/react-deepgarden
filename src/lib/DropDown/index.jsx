import React from 'react';

import classNames from 'classnames';

import OnClickOutside from '../../hoc/OnClickOutside';

import './index.styl';

export default class DropDown extends React.Component {
	handleClickOutside = () => {
		this.props.onClose && this.props.onClose();
	};
	render() {
		return <OnClickOutside onClickOutside={this.handleClickOutside}>
			<div className={classNames('_DropDown', this.props.className)}>
				{this.props.children}
			</div>
		</OnClickOutside>
	}
};