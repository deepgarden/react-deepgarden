import React from 'react';
import {findDOMNode} from 'react-dom';

export default class extends React.Component {
	handleClickOutside = e => {
		if (this.node.contains(e.target)) {
			return false;
		}
		this.props.onClickOutside && this.props.onClickOutside();
	};
	componentDidMount() {
		this.node = findDOMNode(this);
		document.addEventListener('click', this.handleClickOutside);
	}
	componentWillUnmount() {
		document.removeEventListener('click', this.handleClickOutside);
	}
	render() {
		return this.props.children
	}
};